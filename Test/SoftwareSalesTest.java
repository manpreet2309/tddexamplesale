import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SoftwareSalesTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
// R1: Buy  one software package
	@Test
	public void testBuyOneSoftwarePackage()
	{
	}
// R2: Buy 10-19 packages, get 20% discount
	@Test
	 public void testBuy10Package()
	 {	

		SoftwareSales s = new SoftwareSales();
		//Expected result = 20% discount
		double finalPrice = s.calculatePrice(12);
		assertEquals(950.4, finalPrice,0);
		
	 }
	// R3. buy 19-49 .get 30% discount
	@Test
	 public void testBuy20Package()
	 {

		SoftwareSales s = new SoftwareSales();
		//Expected result = 30% discount
		double finalPrice = s.calculatePrice(30);
		assertEquals(2079, finalPrice,0);
		
	 }
	//r4 50 - 99 get 40
	@Test
	 public void testBuy50Package()
	 {
		SoftwareSales s = new SoftwareSales();
		//Expected result = 40% discount
		double finalPrice = s.calculatePrice(60);
		assertEquals(3564, finalPrice,0);
		
	 }
	//R5 100+ 
	@Test
	 public void testBuy100Package()
	 {
		SoftwareSales s = new SoftwareSales();
		//Expected result = 50% discount
		double finalPrice = s.calculatePrice(120);
		assertEquals(5940, finalPrice,0);
		
	 }
	//R6 Quantity <-1 should return -1
	@Test
	 public void testNegativeQuantityPackage()
	 {
		SoftwareSales s = new SoftwareSales();
		double finalPrice = s.calculatePrice(-5);
		assertEquals(-1, finalPrice,0);
		
	 }
}
